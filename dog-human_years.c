/* A program that says dog years in human years
Tip: 1 dog year = 7 human years */

#include <stdio.h>

void dog_human(int x);

int main(void)
{
	int d_age = 5;
	dog_human(d_age);

	return 0;
}

void dog_human(int x)
{
	printf("Dog age in human years is: %d\n", x * 7);
}
