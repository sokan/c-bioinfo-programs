#include <stdio.h>
#include <string.h>
#include <math.h>

struct point { // assigns coordinates of points.
	float x;
	float y;
};

struct triangle { // a, b, c edges 
	struct point edges[3];
};

float distance (struct point, struct point);
float triangle_area (struct triangle);

int main (void)
{

	struct triangle tr1;
	int i;

	for (i = 0; i < 3; i++) {
		printf ("Give x coordinate of point %d: \n", i+1);
		scanf ("%f", &tr1.edges[i].x);
		printf ("Give y coordinate of point %d: \n", i+1);
		scanf ("%f", &tr1.edges[i].y);
	}

	printf ("\nThe surface area of the trianble is: %f\n", triangle_area (tr1));

	return 0;
}

float distance (struct point point1, struct point point2)
{
	return sqrt ( ((point2.x - point1.x) * (point2.x - point1.x)) + ((point2.y - point1.y) * (point2.y - point1.y)) );
}

float triangle_area (struct triangle triangle_name)
{
	float s;
	float e1, e2, e3;

	e1 = distance(triangle_name.edges[0], triangle_name.edges[1]);
	e2 = distance(triangle_name.edges[0], triangle_name.edges[2]);
	e3 = distance(triangle_name.edges[1], triangle_name.edges[2]);
	s = (e1 + e2 + e3) / 2;

	return sqrt ( s * (s - e1) * (s - e2) * (s - e3) );
}
