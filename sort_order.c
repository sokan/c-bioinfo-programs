/* Write a program that sorts a table of number in increasing order */

#include <stdio.h>

int main()
{	
	// Here we create the table of n integers that the user gives
	
	int i;
	int n; //Number of integers/arrays given by the user

	printf("How many integers do you want your table to have? Enter: ");
	scanf("%d", &n);

	int table[n];

	for(i = 0; i < n; i++){
		printf ("Insert integer for index %d: ", i);
		scanf ("%d", &table[i]);
	}
	
	printf("\nThe table you have is:\n");

	for(i = 0; i < n; i++){
		printf("%d\t%d\n", i, table[i]);
	}

	int j; //index as i+1 to compare with index is in order to sort the table.
	int new_ind; //variable to assign the integer that needs to be reallocated.

	for(j = 0; j < (n-1); j++){
		for(i = 0; i < (n-1); i++){
			if(table[i] > table[i+1]){
				new_ind = table[i];
				table[i] = table[i+1];
				table[i+1] = new_ind;
			}
		}
	}

	printf("\nThe sorted table is:\n");
	
	for(i = 0; i < n; i++){
		printf("%d\t%d\n", i, table[i]);
	}

	return 0;
}
