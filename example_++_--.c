#include <stdio.h>

int main(void)
{
	int a=4, b=2, c, d=8;

	printf("a\tb\tc\td\n");

        printf("%d\t%d\t%d\t%d\n", a, b, c, d);

	a = ++b;
	printf("%d\t%d\t%d\t%d\n", a, b, c, d);

	b = a + b--;
        printf("%d\t%d\t%d\t%d\n", a, b, c, d);

	c = 2 * a + d;
        printf("%d\t%d\t%d\t%d\n", a, b, c, d);

	d += (a++) + (--b) + c;
        printf("%d\t%d\t%d\t%d\n", a, b, c, d);

	return 0;
}
