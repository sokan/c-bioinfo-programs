#include <stdio.h>
#include <string.h>

struct person {
	char firstname[20];
	char surname[20];
	char gender;
	int age;
};

int main()
{
	struct person p1 = {"George", "Tsaousis", 'M', 24};
	struct person p2;
	strcpy (p2.firstname, "Kostas");
	strcpy (p2.surname, "Papadopoulos");
	p2.gender = 'M';
	p2.age = 57;
	printf ("%s\n", p1.firstname);
	printf ("%s\n", p2.firstname);

	struct person p3;
	p3 = p2;
	printf ("%s %s %c %d\n", p3.firstname, p3.surname, p3.gender, p3.age);
}
