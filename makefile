CC=gcc

do:
	@echo "Which program do you want to compile?"; \
	read PROGRAM; \
	$(CC) $$PROGRAM.c -o $$PROGRAM.out -lm

run:
	@echo "Which executable do you want to run?"; \
        read PROGRAM; \
	./$$PROGRAM.out


clean:
	rm -r *.out
