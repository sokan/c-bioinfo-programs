#include <stdio.h>

float average(float x, float y) {
	float aver;
	aver = (x + y) / 2.0;
	printf ("The average is: %f\n", aver);
	
	return aver;

}

int main()
{
	float a, b;

	printf("Input first number: ");
	scanf("%f", &a);
	
	printf("Input second number: ");
	scanf("%f", &b);

	average(a, b);

}
