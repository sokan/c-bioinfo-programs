#include <stdio.h>

// Program that prints all odd numbers 0 <= x <= 1000.

int main (void)
{
	int x = 0;
	while ( x < 1000 )
	{
		x = x + 1;
		if (x%2)
		{	
			printf("%d ",x);	
		}
	}
}
