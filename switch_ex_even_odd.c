#include <stdio.h>

int main(void)
{
	int x = 2;

	switch (x % 2) //switch for number "left" after division
	{
		case 0: 
			printf("Number is even\n");
			break;
		
		case 1: 
			printf("Number is odd\n");
			break;
		
		default:
			printf("wrong\n");
			break;
	}
}
