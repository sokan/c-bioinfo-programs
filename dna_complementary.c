/* Add symbolic sequence of DNA and check out its 
 * nucleotidic substance (percentage of each nt base and of GC).
 * Without and with a function (input the dna sequence). */

// Optionals: complementary and motiff/patters in proteins/aa sequence (where and what).

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void dna_copy(char arr[]);

int main()
{
	char dna[1000];

	printf("Please type DNA seq: \n");
	scanf("%s", dna);

	printf("DNA sequence: %s\n", dna);

	dna_copy(dna);

	return 0;
}

void dna_copy(char dna[]){

	int i; //our counter.
	char dna_comp[1000]; // The array which will constitute the complementary DNA chain.
	int dna_len = strlen(dna); //Needed in order to "end" for loop.
	
	for(i = 0; i < dna_len; i++){
		if(dna[i] == 'A'){
			dna_comp[i] = 'T';
		}
		if(dna[i] == 'G'){
			dna_comp[i] = 'C';
		}
		if(dna[i] == 'C'){
			dna_comp[i] = 'G';
		}
		if(dna[i] == 'T'){
			dna_comp[i] = 'A';
		}
	}
	dna_comp[i] = '\0'; //To make sure that it is a string.
	
	printf("The complementary DNA strand is: %s\n", dna_comp);
}
