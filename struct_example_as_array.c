#include <stdio.h>
#include <string.h>

struct person {
	char firstname[20];
	char surname[20];
	char gender;
	int age;
};

int main()
{
	struct person p[40];
	strcpy (p[0].firstname, "Kostas");
	strcpy (p[0].surname, "Papadopoulos");
	p[0].gender = 'M';
	p[0].age = 57;
	printf ("%s\n", p[0].firstname);
	struct person p3;
	p[3] = p[0];
	printf ("%s %s %c %d\n", p[3].firstname, p[3].surname, p[3].gender, p3.age);
}
