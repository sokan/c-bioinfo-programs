#include <stdio.h>

int odd_even(int a);

int main()
{
	int num;
	printf ("Enter number: ");
	scanf ("%d", &num);
	
	odd_even(num);

	return 0;
}

int odd_even(int a){
	
	if (a%2==0) {
		printf ("Number is even");
	}

	else {
		printf("Number is odd");
	}
}
