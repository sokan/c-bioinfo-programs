/* This is a program that produces the RNA transcribed from a 
 * DNA sequence given from a fasta file. */

#include <stdio.h>
#include <string.h>

int main()
{
	int i, j, k, length;
	char ch, dna_seq[100000], rna_seq[100000];

	i = j = k = 0;

	FILE *in_fasta;
	in_fasta = fopen("s_aureus.fasta", "r");

	FILE *sequences;
	sequences = fopen("results.txt", "w");

	if (in_fasta == NULL) {
		printf("Error opening .fasta file\n");
		return -1;
	}

	do{
		ch = fgetc(in_fasta);
	}while (ch != '\n');

	do{
		ch = fgetc(in_fasta);
		if (ch != '\n') {
			dna_seq[i] = ch; 
			i++;
		}
	}while (!feof(in_fasta));
	
	fclose(in_fasta);

	length = i - 1;
	dna_seq[length] = '\0';

	printf("The DNA to be transcribed in 5' -> 3' is: \n%s\nIts length is: %d\n", dna_seq, i - 1);
	fprintf(sequences, "DNA sequence:\n");
	fputs(dna_seq, sequences);

	k = length - 1;
	
	for (j = 0; j <= length; j++){
		rna_seq[j] = dna_seq[k];
		if (rna_seq[j] == 'T'){
			rna_seq[j] = 'U';
		}
		k--;
	}

	rna_seq[length] = '\0';

	printf("\nThe RNA sequence from transcription in 3' -> 5' is: \n%s\n", rna_seq);
	fprintf(sequences, "\nRNA sequence:\n");
	fputs(rna_seq, sequences);

	return 0;

}
