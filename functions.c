#include <stdio.h>

int power(int a, int b);

int main (void)
{
	int x=8;
	int y=2;
	int res=power(x,y);
	printf ("%d\n", res);
}

int power (int base, int n)
{
	int p;
	for (p=1 ; n > 0; n = n-1)
	{
		p = p * base;
	}

	return p;
}
