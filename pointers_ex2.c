#include <stdio.h>

int main()
{
	int a, b, *c, *d, e;

	a = 10;
	b = a * 3;
	c = &a;
	d = &b;
	e = *c + *d;
	*d = a;
	d = &a;
	*c = *d - a % b + *c;

	printf ("a = %d, b = %d, e = %d \n", a, b, e);

	return 0;
}
