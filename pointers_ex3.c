#include <stdio.h>

void f(double, double, double, double*);

int main()
{
	double a = 10;
	double b = 20;
	double c = 40;
	double result;
	double *pr;

	pr = &result;

	f(a,b,c,pr); // f(a,b,c,&result);
	printf ("%f\n", result);
}

void f(double x, double y, double z, double *r)
{
	*r = (x+y+z)/3;
}
