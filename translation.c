#include <stdio.h>
#include <string.h>

/* Function that gets a one liner from our input file */
int fasta_to_seq(char sequence[])
{
	char ch;
	int i = 0;

	FILE *in_fasta;
	in_fasta = fopen("s_aureus.fasta", "r");

	if (in_fasta == NULL) {
		printf("Error opening file");
		return -1;
	}

	do{
		ch = fgetc(in_fasta); // Removes header
	}while (ch != '\n');

	do{
		ch = getc(in_fasta);
		if (ch != '\n') {
			sequence[i] = ch;
			i++;
		}
	}while (!feof(in_fasta));

	fclose(in_fasta);

	sequence[i - 1] = '\0';

	return 0;

}

int main(void)
{
	char ntseq[100000];
	char aaseq[300000];


