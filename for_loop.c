#include <stdio.h>

// Print the number of adding all number from 0 to 1000
int main (void)
{
	int i; //used for counting/incrementing
	int a = 0; //used for addition

	for (i=0; i<=1000; i=i+1)
	{
		a = i + a;
	}
		
	printf ("%d\n",a);
	
	return 0;
}
