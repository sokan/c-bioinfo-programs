/* A program with a table of integers (that the user defines)
 * which contains a function that calculates the sum and average
 * of these integers */

#include <stdio.h>

// Function that prints sum and average of a table via pointers.

void sumave(int *array, int array_size){

	int sum = 0;
	double ave = 0;
	int j;

	for (j = 0; j < array_size; j = j + 1){
		sum = sum + *(array + j); //pointers are table "lines". 
	}

	ave = (double) sum / array_size; // double is needed to print with decimals

	printf ("The sum of all integers is: %d\n", sum);
	printf ("And the average of all integers rounded up to a digit is: %fl\n", ave);
}

int main()
{	
	// Here we create the table of n integers that the user gives
	
	int i;
	int n; //Number of integers/arrays given by the user

	printf ("How many integers do you want your table to have? Enter: ");
	scanf ("%d", &n);

	int table[n];

	for (i = 0; i < n; i = i + 1)
	{
		printf ("Insert integer for index %d: ", i);
		scanf ("%d", &table[i]);
	}
	
	sumave(table, n);
	
	return 0;
}
