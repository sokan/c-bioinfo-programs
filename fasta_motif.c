/* Skip header at a fasta format. 
 * Find protein motiff. Proteins have post-translational modifications: glucozylioses. 
 * Aspagine to be glycozylioted.  Asn(N) + any aminoacid (other than proline) + Ser(S)/Thr(T). 
 * These are the way in which asparagine can be glycozliolated. How many motifs are there? 
 * in which aminoacid places are there? with aa numbering and where asparagine is. */

#include <stdio.h>
#include <string.h>

int main()
{
	FILE *pfile;
	char pdbfile[1000]; //pdbfile == <INPUT>.fasta
	char ch, aaseq[10000], places[10000];
	int i, j, count_motif;
	
	printf("Enter Filename -> ");
	scanf("%s", pdbfile);

	pfile = fopen(pdbfile,"r");

	// if used to make certain the file is opened normally and fine.
	if(pfile == NULL){
		printf("Error opening file.\n");
		return(-1);
	}

	// With the following do loop we "read-out" the first line of the fasta file since we don't use it and keep the rest of the file for usage. 
	do{
		ch = fgetc(pfile);
	}while(ch != '\n');

	fgets(aaseq, 1000, pfile);
	
	printf("\nThe Asparagines(N) that might be glycozyliated based on N-(not P)-S/T motif are: \n");

	for(i = 0; i <= strlen(aaseq); i++){
		if(aaseq[i] == 'N' && aaseq[i+1] != 'P' && (aaseq[i+2] == 'S' || aaseq[i+2] == 'T')){
			count_motif++;
			places[j] = i; j++;
			printf("%d\t", i+1);
		}
	}

	fclose(pfile);

	printf("\nIn total we have %i Asparagines(N) that can be glycozyliated.\n", count_motif);
	
	return 0;
}
