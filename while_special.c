#include <stdio.h>

int main (void)
{
	int f=1 ;
	int n=1 ;
		while (--n) // it'll stop because n=0 which means that while = 0 = false without any print output
	{
		f = f*(n+1);
		printf ("%d %d\n", n, f);
	}
}
