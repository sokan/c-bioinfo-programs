/* Open a pdf file and write all lines beginning with ATOM on another file. */

#include <stdio.h>
#include <string.h>

int main(void)
{
	FILE *in;

	in = fopen("1hfr.pdb", "r");

	if (in == NULL){
		printf ("Error openning file.\n");
		return -1;
	}

	/* Need to have the code scan "in" and when it finds ATOM start printing. */
	
	char line[82]; //80 due to pdb file + 2 (/n and /0)

	while (fgets(line, sizeof(line), in) != NULL) {
		if (strncmp(line, "ATOM", strlen("ATOM")) == 0) // Here we compare each line for the first characters (number of which is the same as the word we're searching for) with a word. 
			fprintf("%s", line);
	}

	fclose(in);

	return 0;
}
