#include <stdio.h>

int power (int a, int b);

int main()
{
	printf("This program calculates and prints the exponent of a base\n\n");
	
	int x;
	int y;

	printf("Give the base: \n");
	scanf("%d", &x);

	printf("Give the exponent: \n");
	scanf ("%d", &y);

	int res = power(x, y);
	printf("The %d exponent of %d is: %d", y, x, res);
}

int power (int base, int n)
{
	if (n == 0)
	{
		return 1;
	}

	else
	{
		return base * power (base, n-1);
	}
}
