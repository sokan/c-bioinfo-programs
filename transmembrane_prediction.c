/* **********************************************************************************************
 * This is an attempt for a program that gets a .fasta reads it, and based on hydrophobic 	*
 * scale by Doolittle it predicts if there are any trasnmembrane parts in the given sequence	*
 * **********************************************************************************************/

#include <stdio.h>
#include <string.h>

/* Structure where we gather all amino_a info required for transmembrane prediction. */
struct aminoacid{ 
	char three[4]; 	// Three letter aminoacid code 
	char one;	// One letter aminoacid code
	float hdnum;	// Hydrophobicity value from Doolittle table for each aminoacid.
};

/* Function that reads hydrophobicity table and assigns it on aminoacid_in[].hdnum */
int hydrophobicity (struct aminoacid aminoacid_in[])
{
	char line[20];
	int i = 0;

	FILE *in_hydro;
	in_hydro = fopen("doolittle_hydrophobe.txt", "r");

	if (in_hydro == NULL) {
		printf("Error openning file <doolittle_hydrophobe.txt>\n");
		return -1;
	}
	
	while (fgets(line, sizeof(line), in_hydro)) { 
			sscanf(line, "%3s %*s %f\n", aminoacid_in[i].three, &aminoacid_in[i].hdnum);
			i++;
	}

	fclose(in_hydro);

	return 0;

}

/* Function that assigns 1-letter code on aminoacid_in[].one */
int three_to_one(struct aminoacid aminoacid_in[])
{
	char line[20];
	int j = 0;

	FILE *in_3code;
	in_3code = fopen("aa_3to1_letter_code.txt", "r");
	
	if (in_3code == NULL) {
		printf("Error openning file <aa_3to1_letter_code.txt>\n");
		return -1;
	}

	// We want to automagically assign 1-letter code to struct
	while (fgets(line, sizeof(line), in_3code)) { 
			sscanf(line, "%*s %c", &aminoacid_in[j].one);
			j++;
	}

	fclose(in_3code);

	return 0;

}

/* Function that scans a given .fasta and assigns the aa sequence on one consecuritve 
 * string without the header */
int fasta_to_string(char sequence[])
{
	int k = 0;
	char ch;

	FILE *in_fasta;
	in_fasta = fopen("P0DTC2.fasta", "r");

	if (in_fasta == NULL) {
		printf("Error opening .fasta file\n");
		return -1;
	}

	do{
		ch = fgetc(in_fasta);
	}while (ch != '\n');

	do{
		ch = getc(in_fasta);
		if (ch != '\n') {
			sequence[k] = ch;
			k++;
		}
	}while (!feof(in_fasta));

	fclose(in_fasta);

	sequence[k - 1] = '\0'; //Finalizing the string by adding \0 as final character.

	return 0;

}
/* Here we create a new array that contains the hydrophobicity values and calculates
 * the sum of hydrophobicity for all aminoacids per window parsing.
 * i.e. Hydrophobicity "dicionary" */
//int hydrophobic_assign(struct amionoacid aminoacid_in[], char sequence_to_scan[]);
//
int main(void)
{
	struct aminoacid aminoacid_in[20];
	char aaseq[100000];

	hydrophobicity (aminoacid_in);
	three_to_one (aminoacid_in);
	fasta_to_string (aaseq);

	printf("Your sequence is: %s\n", aaseq);
	printf("Sequence length: %ld residues.\n", strlen(aaseq));
	//printf("%lf\t%c\t%s\n", aminoacid_in[0].hdnum, aminoacid_in[0].one, aminoacid_in[0].three);

	/* Assign hydrophobic number in a new array for each aminoacid in our sequence.
	 * hydrophobicseq is now our sequence but with doolittle hydrophobicity numbers
	 * instead of aminoacids. */
	int l = 0, m = 0;
	float hydrophobicseq[100000];
	
	for (l = 0; l < strlen(aaseq); l++) {
		for (m = 0; m <= 20; m++) {
			if (aaseq[l] == aminoacid_in[m].one) {
				hydrophobicseq[l] = aminoacid_in[m].hdnum;
			}
		}
	}
	
	//printf("check: %lf, check2: %lf\n", hydrophobicseq[2], aminoacid_in[0].hdnum);

	/* Scan out "hydrophobic number sequence" with a window where we do the
	 * designated sums to figure out hydrophobicity as a whole. */
	int n = 0, p = 0, window = 11;
	float total_window_hydro= 0;
	float sum[100000];

	for (n = 0; < (strlen(aaseq) - window); n++) {
		for (p = n; p < window + n; p++) {
			total_window_hydro = total_window_hydro + hydrophobicseq[p];
		}
		sum[n] = total_window_hydro / window;
		total_window_hydro = 0; //initialize with 0 for next window calculations.
	}

	//printf("test numbers: %f\n", sum[0]);

	// Something more left but I need to think about what that is. Probably how to assign the sum to the middle aminoacid and print that sum with each amoniacid and mention if it's hydrophobic (if sum[i] > 0 printf(T) else printf(-)).

	return 0;

}
