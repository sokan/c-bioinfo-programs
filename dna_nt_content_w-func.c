/* Add symbolic sequence of DNA and check out its 
 * nucleotidic substance (percentage of each nt base and of GC).
 * Without and with a function (input the dna sequence). */

// Optionals: complementary and motiff/patters in proteins/aa sequence (where and what).

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void dna_count(char arr[]);

int main()
{
	char dna[1000];

	printf("Please type DNA seq: \n");
	scanf("%s", dna);

	printf("DNA sequence: %s\n", dna);

	dna_count(dna);

	return 0;
}

void dna_count(char dna[]){

	int i; //our counter.
	double dna_len = strlen(dna);
	double count_g, count_a, count_t, count_c;
	count_g = count_a = count_t = count_c = 0; //initializing counters of nucleotides.

	for(i = 0; i < dna_len; i++){
		if(dna[i] == 'A'){
			count_a = count_a + 1;
		}
		if(dna[i] == 'G'){
			count_g = count_g + 1;
		}
		if(dna[i] == 'C'){
			count_c = count_c + 1;
		}
		if(dna[i] == 'T'){
			count_t = count_t + 1;
		}
	}

	printf("DNA length: %d\n", (int)dna_len);

	printf("%% of C in DNA is: %lf\n", (count_c / dna_len) * 100);
	printf("%% of A in DNA is: %lf\n", (count_a / dna_len) * 100);
	printf("%% of G in DNA is: %lf\n", (count_g / dna_len) * 100);
	printf("%% of T in DNA is: %lf\n", (count_c / dna_len) * 100);
	printf("%% of GC in DNA is: %lf\n", ((count_c + count_g) / dna_len) * 100);
}
