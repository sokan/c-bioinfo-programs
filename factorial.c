#include <stdio.h>

int factorial(int a);

int main()
{
	int x;

	printf ("Insert integer: ");
	scanf ("%d", &x);

	int res = factorial(x);
	printf ("The factorial of %d is: %d", x, res);

	return 0;
}

int factorial(int num)
{
	if (num == 0)
	{
		return 1;
	}
	else
	{
		return num * factorial (num - 1);
	}
}
