/* **************************************************************************** *
 * 	Calculate the distance between 2 atoms that a user defines via input 	*
 * 	The distance shall be between 2 C_a carbons.				*
 * 	We want as an output: the name and place of the aminoacid, x, y, z  	*
 * 	coordinates.								*
 * **************************************************************************** */

#include <stdio.h>
#include <string.h>
#include <math.h>

/* A "database" of our atoms. */
struct atomid { 
	char type[4]; 	// C, CA, N, C, ...
	char name[4]; 	// LYS, PHE, GLU, ...
	char chain[2];	// A,B, ... Chain identifier
	int naa; 	// This is the number of the aa that the user wants to check inside .pdb
	float x, y, z; 	// Coordinates of the desigates atom.
};

/* Function for calculating the distance of wanted atoms. */
float atm_distance(struct atomid atom_id[], int i, int j)
{
	float x1x2 = atom_id[i].x - atom_id[j].x;
	float y1y2 = atom_id[i].y - atom_id[j].y;
	float z1z2 = atom_id[i].z - atom_id[j].z;
	return sqrt(x1x2 * x1x2 + y1y2 * y1y2 + z1z2 * z1z2);
}

/* Function for scanning out ATOM "database" for "CA" types, which we use for calculating the distance of the aminoacids. */
float atoms_parsing(struct atomid atom_id[], int user_input, int atom_lines)
{	
	int l = 0;

	for (l = 0; l <= atom_lines; l++) {
		if ((atom_id[l].naa == user_input) && (strcmp(atom_id[l].type, "CA") == 0)) {
			return (l); // +1 because atom_id[] starts from 0 and in the file it ATOM numbers starts from 1
		}
	}
	return -1; //In case it does not find "CA" or "ATOM" atoms_parsing gives -1. See line 83 for usage.
}

int main()
{	
	printf ("##### This program calculates the distance between the C_A carbon atoms of 2 aminoacids that the user wants, from the designated .pdb file #####\n\n");

	FILE *in;
	char pdbfile[1000000];

	printf("Enter the pdb file (<NAME>.pdb) that you want to study: ");
	scanf("%s", pdbfile);
	in = fopen(pdbfile, "r");

	/* Check if input file has been opened sucessfully */
	if (in == NULL) {
		printf ("Error openning file.\n");
		return -1;
	}

	/* Here we build our ATOMs ID "database" */
	
	struct atomid atom_id[100000];	// Declare all possible atom_ids for out .pdb.
	int k = 0;
	
	char line[82];	// We define the scanning of lines.

	while (fgets(line, sizeof(line), in)) {
		if (strncmp(line, "ATOM", strlen("ATOM")) == 0){
			sscanf(line, "ATOM %*d %s %s %s %d %f %f %f", atom_id[k].type, atom_id[k].name, atom_id[k].chain, &atom_id[k].naa, &atom_id[k].x, &atom_id[k].y, &atom_id[k].z);
		k++;
		}
	}

	fclose(in);
	
	int aa_in1, aa_in2, first_atm, second_atm; 
	float final_dist;
	aa_in1 = aa_in2 = first_atm = second_atm = final_dist = 0;

	printf ("Insert the number of the first aminoacid: \n");
	scanf ("%d", &aa_in1);
	first_atm = atoms_parsing(atom_id, aa_in1, k);
	
	printf ("Insert the number of the second aminoacid: \n");
	scanf ("%d", &aa_in2);
	second_atm = atoms_parsing(atom_id, aa_in2, k);

	/* We check if the inputs of the user are in the ATOM list and also if they include "CA" */
	if ((first_atm == -1) || (second_atm == -1)) {
		printf("The aminoacids you chose have no C_A or they do not exist.\n");
		return -1;
	}

	final_dist = atm_distance(atom_id, first_atm, second_atm);

	printf("The aminoacids you specidied are %s in chain %s (%d), and %s in chain %s (%d).\nThe distance of their C_A is: %f Angstrom.\n",atom_id[first_atm].name, atom_id[first_atm].chain, atom_id[first_atm].naa, atom_id[second_atm].name, atom_id[second_atm].chain, atom_id[second_atm].naa, final_dist);

        return 0;
}
