/* Code that will require input for Fahrenhei, Kelvin or Celcius and 
 * a specific temperature (e.g. 26.7). Depending of what we give we want the program
 * to output the other 2 temperate formats. 
 * Tip: use switch on conversion formulae. */

#include <stdio.h>

float f2ck(float fahr)
{
	float celc, kelv;
	celc = ((fahr - 32) * 5 ) / 9;
	kelv = celc + 273.15;
	printf ("Your temperature in degrees Celcius is %f and in Kelvin is %f\n", celc, kelv);
}

float c2fk(float celc)
{
	float fahr, kelv;
	fahr = ((celc * 9) / 5) + 32;
	kelv = (celc + 273.15);
	printf ("Your temperature in degrees Fahrenheit is %f and in Kelvin is %f\n", fahr, kelv);

}

float k2cf (float kelv)
{
	float celc, fahr;
	fahr = ((kelv - 273.15) * 9) / 5 + 32;
	celc = kelv - 273.15;
	printf ("Your temperature in degrees Celcius is %f and in Fahrenheit is %f\n", celc, fahr);
}

void main (void)
{
	int input;
	float temp;

	printf ("**** This is a temperature conversion code. ****\n");

	printf ("Enter termerature unit (0 for Celsius, 1 for Fahrnheit, 2 for Kelvin): ");
	scanf ("%d", &input);

	printf ("Enter the temperature value: ");
	scanf ("%f", &temp);

	switch (input) {
		case 0: c2fk (temp);
			break;
		case 1:	f2ck (temp);
			break;
		case 2: k2cf (temp);
			break;
	}
}
