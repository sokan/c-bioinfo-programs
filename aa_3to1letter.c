#include <stdio.h>

struct threetoone{ 
	char three[4];
	char one[2];
};


int main()
{
	FILE *in;
	in = fopen("aa_3to1_letter_code.txt", "r");

	struct threetoone threetoone_in[1000];
	char line[20];
	int i = 0;

	while (fgets(line, sizeof(line), in)) { 
			sscanf(line, "%3s %*s %s\n", threetoone_in[i].three, threetoone_in[i].one);
			i++;
	}
	printf ("%s\t%s\n", threetoone_in[2].three, threetoone_in[2].one);

	return 0;
}
