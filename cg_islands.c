/* ************************************************************************************************
 * Find CpG islands on the genome. 								  *
 * CpG islands: high frequency of CG sites. 							  *
 *												  *
 * High freq rules: 										  *
 * - at least 200bp length of our region. (window slide)					  * 
 * - GC percentage > 0.5 (50%).									  *
 * - observed-to-expected CpG ratio > 0.6 (60%).						  *
 *   - expected == (nC * nG)/window length (in our case 200bp)					  *
 *												  *
 * Suggested output: CpG island detected in region 412 to 899 (Obs/Exp = 0.62 and %GC = 50.50)	  *
 * 		     CpG island 		   1800 to 1999 (Obs/Exp = ...)			  *
 * ************************************************************************************************/

#include <stdio.h>
#include <string.h>

void cpg_isle(char sequence[]);

int main()
{
	FILE *pfile;
	char genfile[50];
	char head_off, seqscan, genstr[100000], geneseq[100000], regions[100000];
	int i = 0;

	printf("Enter GeneFile -> ");
	scanf("%s", genfile);
	pfile = fopen(genfile,"r");

	if(pfile == NULL){
		printf("Error opening file.\n");
		return -1;
	}

	// Removing the header:
	do{
		head_off = fgetc(pfile);
	}while(head_off != '\n');
	
	// Making fasta sequence a 1-liner:
	do{
		seqscan = getc(pfile); // Scan char-by-char the sequence
		if(seqscan != '\n'){
			genstr[i] = seqscan;
			i++;
		}
	}while(!feof(pfile));
	
	// Finalize the string creation by adding \0 in the end.
	genstr[i - 1]='\0'; // i - 1 is used because in do loop i was increased by one, before the end of file, so we have an "exess" i.

	fclose(pfile);

	printf("Your sequence is %ld nucleotides long.\n", strlen(genstr));
	
	cpg_isle(genstr);

	return 0;
}

void cpg_isle(char sequence[]) // Write a function that will count C and G in sequence.
{
	double count_g = 0, count_c = 0, count_cg = 0, exp_cg = 0, cg_perc = 0, ratio = 0, window_size = 200;
	int j = 0, k = 0;

	for (j = 0; j < (strlen(sequence) - window_size); j++){
		
		count_g = count_c = count_cg = 0; // Initialize all "C, G counts" after each window slide.

		for (k = j; k < (j + window_size); k++){
			if ((sequence[k] == 'C') || (sequence[k] == 'c'))
				count_c++;
			if ((sequence[k] == 'G') || (sequence[k] == 'g'))
				count_g++;
			if (((sequence[k] == 'C') || (sequence[k] == 'c')) && ((sequence[k+1] == 'G') || (sequence [k+1] == 'g')))
				count_cg++;
		}

		exp_cg = (count_c * count_g) / window_size;
		ratio = count_cg / exp_cg;
		cg_perc = (count_c + count_g) / window_size;

		if ((ratio > 0.60) && (cg_perc > 0.50))
			printf ("CpG island detected in region %d to %d (Obs/Exp = %f and %%GC = %f)\n", j+ 1, k + 1, ratio, cg_perc);
	}
}
