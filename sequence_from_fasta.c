#include <stdio.h>
#include <string.h>

int main()
{
	FILE *in;
	char aaseq[100000], ch, final_string[100000];
	int i = 0;

	in = fopen("P0DTC2.fasta", "r");

	if (in == NULL) {
		printf("Error opening file.\n");
		return -1;
	}

	do{
		ch = fgetc(in);
	}while (ch != '\n');

	while (!feof(in)) {
		ch = getc(in);
		if ( ch != '\n' ){
			aaseq[i] = ch;
			i++;
		}
	}

	aaseq[i - 1] = '\0'; //finalize sequence by adding \0 as the final character.

	printf("%s/n", aaseq);

	return 0;
}
