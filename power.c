#include <stdio.h>

double power(double base, int n);

int main()
{
	int a, b;

	printf ("Type in the base: ");
	scanf ("%d", &a);

	printf ("Type in the power: ");
	scanf ("%d", &b);

	double res = power(a, b);

	printf ("The result of %d to the power of %d is: %f", a, b, res);

	return 0;
}

double power(double base, int n)
{
	if ( n == 0)
	{
		return 1;
	}
	else if (n > 0)
	{
		return base * power (base, n-1);
	}

	else 
	{
		return (1 / base) * power (base, n+1);
	}
}
